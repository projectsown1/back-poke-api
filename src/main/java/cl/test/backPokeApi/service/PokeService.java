package cl.test.backPokeApi.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import cl.test.backPokeApi.dto.PokeSpecieDTO;
import cl.test.backPokeApi.model.PokeEvolution;
import cl.test.backPokeApi.model.PokeEvolve;
import cl.test.backPokeApi.model.PokeModel;
import cl.test.backPokeApi.model.PokeSpecie;

@Service
public class PokeService {
	
	@Autowired
	private RestTemplate restTemplate;
	
	public PokeSpecie getDataPoke(String url) {
        PokeSpecieDTO responseDTO = restTemplate.getForObject(url, PokeSpecieDTO.class);
        
        List<PokeModel> pokemonDataArray = new ArrayList<>();
        
        for (PokeSpecie.PokemonResult pokemon : responseDTO.getResults()) {
            String pokeName = pokemon.getName();
            
            PokeModel pokeData = createPokeModel(pokeName);
            
            pokemonDataArray.add(pokeData);
        }
        
        PokeSpecie response = new PokeSpecie();
        response.setNext(responseDTO.getNext());
        response.setPrevious(responseDTO.getPrevious());
        response.setPokeData(pokemonDataArray);
        
        return response;
    }
    
    private PokeModel createPokeModel(String pokemonName) {
        PokeModel responseImg = restTemplate.getForObject("https://pokeapi.co/api/v2/pokemon/" + pokemonName, PokeModel.class);
        PokeEvolution responseEvolution = restTemplate.getForObject("https://pokeapi.co/api/v2/pokemon-species/" + pokemonName, PokeEvolution.class);
        PokeEvolve responseEvolve = restTemplate.getForObject(responseEvolution.getEvolution_chain().getUrl(), PokeEvolve.class);
        
        List<PokeModel.Evolution> evolutionList = new ArrayList<>();
        collectEvolutions(responseEvolve.getChain(), evolutionList);
        
        PokeModel pokemonData = new PokeModel();
        pokemonData.setId(responseImg.getId());
        pokemonData.setName(pokemonName);
        pokemonData.setImage(responseImg.getSprites().getOther().getHome().getFront_default());
        pokemonData.setAbilities(responseImg.getAbilities());
        pokemonData.setMoves(responseImg.getMoves());
        pokemonData.setStats(responseImg.getStats());
        pokemonData.setEvolution(evolutionList);
        
        return pokemonData;
    }
    
	private void collectEvolutions(PokeEvolve.Chain chain, List<PokeModel.Evolution> evolutionList) {
	    if (chain != null) {
	    	PokeModel.Evolution evolution = new PokeModel.Evolution();
	    	PokeModel responseImg = restTemplate.getForObject("https://pokeapi.co/api/v2/pokemon/" + chain.getSpecies().getName(), PokeModel.class);
	    	
	    	evolution.setName(chain.getSpecies().getName());
	    	evolution.setImage(responseImg.getSprites().getOther().getHome().getFront_default());
	    	evolutionList.add(evolution);
	        for (PokeEvolve.EvolvesTo evolvesTo : chain.getEvolves_to()) {
	            collectEvolutions(evolvesTo, evolutionList);
	        }
	    }
	}

	private void collectEvolutions(PokeEvolve.EvolvesTo evolvesTo, List<PokeModel.Evolution> evolutionList) {
	    if (evolvesTo != null) {
	    	PokeModel.Evolution evolution = new PokeModel.Evolution();
	    	PokeModel responseImg = restTemplate.getForObject("https://pokeapi.co/api/v2/pokemon/" + evolvesTo.getSpecies().getName(), PokeModel.class);
	    	
	    	evolution.setName(evolvesTo.getSpecies().getName());
	    	evolution.setImage(responseImg.getSprites().getOther().getHome().getFront_default());
	    	evolutionList.add(evolution);
	        for (PokeEvolve.EvolvesTo nextEvolvesTo : evolvesTo.getEvolves_to()) {
	            collectEvolutions(nextEvolvesTo, evolutionList);
	        }
	    }
	}
}
