package cl.test.backPokeApi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cl.test.backPokeApi.model.PokeSpecie;
import cl.test.backPokeApi.service.PokeService;

@RestController
@RequestMapping("/api")
public class PokeController {
	
	@Autowired
	private PokeService pokeService;
	
	@GetMapping("/poke")
	public PokeSpecie getData(@RequestParam(required = false) String url) {
		if (url == null || url.isEmpty()) {
        	url = "https://pokeapi.co/api/v2/pokemon-species";
        }
        return pokeService.getDataPoke(url);
    }
}
