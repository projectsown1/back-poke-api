package cl.test.backPokeApi.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import cl.test.backPokeApi.model.PokeSpecie;
import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class PokeSpecieDTO {
    private String next;
    private String previous;
    private List<PokeSpecie.PokemonResult> results;
}
