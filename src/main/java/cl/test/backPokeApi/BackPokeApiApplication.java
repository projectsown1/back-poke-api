package cl.test.backPokeApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackPokeApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackPokeApiApplication.class, args);
	}

}
