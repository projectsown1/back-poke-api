package cl.test.backPokeApi.model;

import java.util.List;

import org.apache.catalina.startup.HomesUserDatabase;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class PokeModel {

	private int id;
    private String name;
    private String image;
    private List<Evolution> evolution;
    private Sprites sprites;
    private List<Abilities> abilities;
    private List<Moves> moves;
    private List<Stats> stats;
    
    @Getter
    @Setter
    public static class Sprites {
        private Other other;

        @Getter
        @Setter
        public static class Other {
            private Home home;

            @Getter
            @Setter
            public static class Home {
                private String front_default;
            }
        }
    }
    
    @Getter
    @Setter
    public static class Abilities {
        private Ability ability;
        
        @Getter
        @Setter
        public static class Ability {
            private String name;
        }
    }
    
    @Getter
    @Setter
    public static class Stats {
        private Stat stat;
        private String base_stat;
        
        @Getter
        @Setter
        public static class Stat {
            private String name;
        }
    }
    
    @Getter
    @Setter
    public static class Evolution {
        private String name;
        private String image;
    }
    
    @Getter
    @Setter
    public static class Moves {
        private Move move;
        
        @Getter
        @Setter
        public static class Move {
            private String name;
        }
    }

}