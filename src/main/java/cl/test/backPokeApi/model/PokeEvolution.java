package cl.test.backPokeApi.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class PokeEvolution {
	private EvolutionChain evolution_chain;

    @Getter
    @Setter
    public static class EvolutionChain {
        private String url;
    }
}
