package cl.test.backPokeApi.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class PokeEvolve {
	private Chain chain;
	
	@Getter
    @Setter
    public static class Chain {
        private List<EvolvesTo> evolves_to;
        private Species species;
    }

    @Getter
    @Setter
    public static class EvolvesTo {
        private List<EvolvesTo> evolves_to;
        private Species species;
    }

    @Getter
    @Setter
    public static class Species {
        private String name;
    }
}
