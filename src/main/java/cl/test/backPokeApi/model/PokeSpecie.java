package cl.test.backPokeApi.model;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class PokeSpecie {
    private String next;
    private String previous;
	private List<PokemonResult> results;
    private List<PokeModel> pokeData;

    @JsonIgnoreProperties(ignoreUnknown = true)
    @Getter
    @Setter
    public static class PokemonResult {
        private String name;
    }
}
